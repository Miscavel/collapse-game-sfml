#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>
#include <time.h>
#include <Windows.h>
#include <math.h>
#include <sstream>

void deleteRow(std::vector<std::vector<int>> &FullBar, int ColSize)
{
	while(true)
	{
		int empty_check = 0;
		for(int j = 0; j < ColSize; j++)
		{
			if(FullBar[0][j] == 0)
			{
				empty_check++;
			}
		}
		if(empty_check == ColSize)
		{
			for(int i = 0; i < FullBar.size()-1; i++)
			{
				for(int j = 0; j < ColSize; j++)
				{
					FullBar[i][j] = FullBar[i+1][j];
				}
			}
			FullBar.pop_back();
			if(FullBar.size() == 0)
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
}

void blockAdjust(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize, int mouseY, int mouseX)
{
	//VerticalAdjust
	if(FullBar.size() > 0)
	{
		if((ceil(double(mouseY)/32)-1) < (RowSize) && (ceil(double(mouseX)/32)-1) < (ColSize) && (ceil(double(mouseY)/32)-1) >= (RowSize - FullBar.size()))
		{
			for(int i = FullBar.size()-1; i > 0; i--)
			{
				for(int j = 0; j < ColSize; j++)
				{
					for(int k = i-1; FullBar[i][j] == 0 && k >= 0; k--)
					{
						FullBar[i][j] = FullBar[k][j];
						FullBar[k][j] = 0;
					}
				}
			}
	
			//HorizontalAdjust1
			for(int j = ceil(double(double(ColSize)/2)); j > 0; j--)
			{
				for(int k = j-1; FullBar[FullBar.size()-1][j] == 0 && k >= 0; k--)
				{
					for(int i = 0; i < FullBar.size(); i++)
					{
						FullBar[i][j] = FullBar[i][k];
						FullBar[i][k] = 0;
					}
				}
			}

			//HorizontalAdjust2
			for(int j = ceil(double(double(ColSize)/2))+1; j < ColSize-1; j++)
			{
				for(int k = j+1; FullBar[FullBar.size()-1][j] == 0 && k < ColSize; k++)
				{
					for(int i = 0; i < FullBar.size(); i++)
					{
						FullBar[i][j] = FullBar[i][k];
						FullBar[i][k] = 0;
					}
				}
			}
		}
	}

}

bool checkTile(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize, int positionY, int positionX, int &score)
{
	std::vector<int> PositionY_Bank;
	std::vector<int> PositionX_Bank;
	
	int core_tile = FullBar[positionY][positionX], tileMatch = 1;


	if(core_tile != 0)
	{
		FullBar[positionY][positionX] = -1;
		PositionY_Bank.push_back(positionY);
		PositionX_Bank.push_back(positionX);
	
		while(PositionX_Bank.size() != 0 && PositionY_Bank.size() != 0)
		{
			int indexy, indexx;
			indexy = PositionY_Bank[PositionY_Bank.size()-1];
			indexx = PositionX_Bank[PositionX_Bank.size()-1];
			PositionY_Bank.pop_back();
			PositionX_Bank.pop_back();
			if(indexy - 1 >= 0)
			{
				if(FullBar[indexy - 1][indexx] == core_tile)
				{
					tileMatch++;
					FullBar[indexy - 1][indexx] = -1;
					PositionY_Bank.push_back(indexy - 1);
					PositionX_Bank.push_back(indexx);
				}
			}
			if(indexy + 1 < FullBar.size())
			{
				if(FullBar[indexy + 1][indexx] == core_tile)
				{
					tileMatch++;
					FullBar[indexy + 1][indexx] = -1;
					PositionY_Bank.push_back(indexy + 1);
					PositionX_Bank.push_back(indexx);
				}
			}
			if(indexx - 1 >= 0)
			{
				if(FullBar[indexy][indexx - 1] == core_tile)
				{
					tileMatch++;
					FullBar[indexy][indexx - 1] = -1;
					PositionY_Bank.push_back(indexy);
					PositionX_Bank.push_back(indexx - 1);
				}
			}
			if(indexx + 1 < ColSize)
			{
				if(FullBar[indexy][indexx + 1] == core_tile)
				{
					tileMatch++;
					FullBar[indexy][indexx + 1] = -1;
					PositionY_Bank.push_back(indexy);
					PositionX_Bank.push_back(indexx + 1);
				}
			}
		}
	
		if(tileMatch >= 3)
		{
			score = score + 10 + (tileMatch - 3) * 10;
			return true;
		}
	}
	return false;
}

void collapseTile(std::vector<std::vector<int>> &FullBar, int ColSize, int RowSize, int mouseY, int mouseX, int &score)
{
	bool collapse;
	int temp;
	
	if(FullBar.size() > 0)
	{
		if((ceil(double(mouseY)/32)-1) < (RowSize) && (ceil(double(mouseX)/32)-1) < (ColSize) && (ceil(double(mouseY)/32)-1) >= (RowSize - FullBar.size()))
		{
			temp = FullBar[(ceil(double(mouseY)/32)-1) - (RowSize - FullBar.size())][ceil(double(mouseX)/32)-1];
			collapse = checkTile(FullBar,ColSize,RowSize,(ceil(double(mouseY)/32)-1) - (RowSize - FullBar.size()),ceil(double(mouseX)/32)-1,score);
			for(int i = 0; i < FullBar.size(); i++)
			{
				for(int j = 0; j < ColSize; j++)
				{
					if(FullBar[i][j] == -1)
					{
						if(collapse == true)
						{
							FullBar[i][j] = 0;
						}
						else
						{
							FullBar[i][j] = temp;
						}
					}
				}
			}
		}
	}
}

void main()
{
	FreeConsole();
	int color_amount = 3;
	int block_amount = 0;
	int stage = 1;
	int score = 0;
	srand(time(NULL));
	bool pause = false;
	float Speed = 0.14;
	int ColSize = 15;
	int RowSize = 15;
	std::vector<int> ColBar;
	std::vector<std::vector<int>> FullBar;
	
	sf::RectangleShape Block;
	Block.setSize(sf::Vector2f(32,32));
	Block.setOutlineColor(sf::Color(0,0,0,255));
	Block.setOutlineThickness(0.7);

	sf::Font NotifFont;
	sf::Text GameOver_Text, Score_Text, Stage_Text;
	NotifFont.loadFromFile("Lumberjack.otf");
	GameOver_Text.setFont(NotifFont);
	GameOver_Text.setCharacterSize(32);
	GameOver_Text.setColor(sf::Color(255,255,255,255));
	GameOver_Text.setPosition(616, 192);
	GameOver_Text.setString("Game Over");

	Score_Text.setFont(NotifFont);
	Score_Text.setCharacterSize(32);
	Score_Text.setColor(sf::Color(255,255,255,255));
	Score_Text.setPosition(616, 0);
	
	Stage_Text.setFont(NotifFont);
	Stage_Text.setCharacterSize(32);
	Stage_Text.setColor(sf::Color(255,255,255,255));
	Stage_Text.setPosition(616, 64);

	sf::Time BlockSpawn;
	sf::Clock BlockSpawnClock;

	sf::Vector2i screenDimension(800,600);
	sf::RenderWindow Window(sf::VideoMode(screenDimension.x, screenDimension.y),"");
	Window.setTitle("Collapse");
	while(Window.isOpen())
	{
		std::ostringstream convert_score, convert_stage;

		BlockSpawn = BlockSpawnClock.getElapsedTime();
		sf::Event Event;
		while(Window.pollEvent(Event))
		{
			if(Event.type == sf::Event::Closed)
			{
				Window.close();
			}
			if(Event.type == sf::Event::Resized)
			{
				Window.setView(sf::View(sf::FloatRect(0, 0, Event.size.width, Event.size.height)));
			}
			if(Event.type == sf::Event::MouseButtonPressed)
			{
				if(Event.mouseButton.button == sf::Mouse::Left)
				{
					collapseTile(FullBar,ColSize,RowSize,Event.mouseButton.y,Event.mouseButton.x,score);
					blockAdjust(FullBar,ColSize,RowSize,Event.mouseButton.y,Event.mouseButton.x);
				}
			}
		}
		convert_score << score;
		Score_Text.setString("Score : "+convert_score.str());
		convert_stage << stage;
		Stage_Text.setString("Stage : "+convert_stage.str());
		Window.draw(Score_Text);
		Window.draw(Stage_Text);
		if(pause == false)
		{
			if(FullBar.size() > 0)
			{
				deleteRow(FullBar,ColSize);
			}

			if(BlockSpawn.asSeconds() > Speed)
			{
				int block;
				block_amount++;
				block = rand()%(color_amount)+1;
				ColBar.push_back(block);
				BlockSpawnClock.restart();
			}

			for(int i = FullBar.size()-1, k = 1; i >= 0; i--, k++)
			{
				for(int j = 0; j < ColSize; j++)
				{
					Block.setPosition(j * 32, (RowSize-k) * 32);
					if(FullBar[i][j] == 0)
					{
						Block.setFillColor(sf::Color(0,0,0,0));
					}
					else if(FullBar[i][j] == 1)
					{
						Block.setFillColor(sf::Color(153,76,0,255));
					}
					else if(FullBar[i][j] == 2)
					{
						Block.setFillColor(sf::Color(255,128,0,255));
					}
					else if(FullBar[i][j] == 3)
					{
						Block.setFillColor(sf::Color(0,255,128,255));
					}
					else if(FullBar[i][j] == 4)
					{
						Block.setFillColor(sf::Color(255,153,153,255));
					}
					Window.draw(Block);
				}
			}

			for(int i = 0; i < ColBar.size(); i++)
			{
				Block.setPosition(i * 32, RowSize * 32);
				if(ColBar[i] == 1)
				{
					Block.setFillColor(sf::Color(153,76,0,100));
				}
				else if(ColBar[i] == 2)
				{
					Block.setFillColor(sf::Color(255,128,0,100));
				}
				else if(ColBar[i] == 3)
				{
					Block.setFillColor(sf::Color(0,255,128,100));
				}
				else if(ColBar[i] == 4)
				{
					Block.setFillColor(sf::Color(255,153,153,100));
				}
				Window.draw(Block);
			}

			if(ColBar.size() == ColSize)
			{
				FullBar.push_back(ColBar);
				ColBar.clear();
			}

			if(FullBar.size() > RowSize)
			{
				pause = true;
				Window.draw(GameOver_Text);
				Window.display();
			}
			else
			{
				Window.display();
				Window.clear();
			}

			if(block_amount == ColSize * 20)
			{
				block_amount = 0;
				stage++;
				if(Speed <= 0.08)
				{
					if(color_amount < 4)
					{
						color_amount++;
						Speed = 0.14;
					}
				}
				else
				{
					Speed = Speed - 0.02;
				}
			}
		}
	}
}

