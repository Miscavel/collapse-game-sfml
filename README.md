# Collapse Game SFML

Collapse Puzzle Game made in SFML-2.3.2

Project compiled using Visual Studio 2010 32-bit

Gameplay :
1. Click on a tile that is linked with 2 or more other tiles of the same color to destroy them.
2. Player is rewarded with 10 points for destroying 3 tiles, and another 10 points for every extra tile destroyed.\
    E.g. 3 tiles = 10 pts\
        4 tiles = 20 pts\
        5 tiles = 30 pts\
        6 tiles = 40 pts\
        ...\
        3+n tiles = (10 + n * 10) pts
3. After every 20 rows of tiles, the stage level will increase by one, increasing the speed at which the tiles are generated.
4. At stage 5, the speed resets to the default speed, and the color variety increases by 1.


![Gameplay Video](Media/trailer.mp4)